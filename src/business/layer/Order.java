package business.layer;

import java.io.Serializable;

public class Order implements Serializable{
	
	private int orderId;
	private int date;
	private int table;
	private String orderString;
	public int getOrderId() {
		return orderId;
	}
	public void setOrderId(int orderId) {
		this.orderId = orderId;
	}
	public int getDate() {
		return date;
	}
	public void setDate(int date) {
		this.date = date;
	}
	public int getTable() {
		return table;
	}
	public void setTable(int table) {
		this.table = table;
	}
	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + date;
		result = prime * result + orderId;
		result = prime * result + table;
		return result;
	}
	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		Order other = (Order) obj;
		if (date != other.date)
			return false;
		if (orderId != other.orderId)
			return false;
		if (table != other.table)
			return false;
		return true;
	}
	
	public void setStringOrder(String orderString) {
		this.orderString = orderString;
	}
	
	public String getStringOrder() {
		return orderString;
	}
	
	
}
