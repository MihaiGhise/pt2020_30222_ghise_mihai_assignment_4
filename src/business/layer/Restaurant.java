package business.layer;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import data.FileWriter;
import presentation.AdministratorGUI;
import presentation.WaiterGUI;

public class Restaurant implements Serializable {
	
	
	/**
	 * Collections used to process the data
	 */
	Map<Order, List<MenuItem>> map = new HashMap<Order, List<MenuItem>>();
	List<MenuItem> menuItems = new ArrayList<MenuItem>();
	List<Order> orders = new ArrayList<Order>();
	WaiterGUI waiter;
	
	public Restaurant(List<MenuItem> menuItems, WaiterGUI waiter, String fileName) {
		this.waiter = waiter;
		this.menuItems = waiter.getMenuItems();
		orders = waiter.getOrders();
		setMenuItems();
		saveData(fileName);
	}
	
	/**
	 * Method for setting the menu items from the waiter class
	 */
	public void setMenuItems() {
		for(Order o : orders) {
			List<String> itemsString = waiter.parse(o.getStringOrder(), "\\S+");
			List<MenuItem> items = new ArrayList<MenuItem>();
			for(String s : itemsString) {
				for(MenuItem it : menuItems) {
					if(s.equals(it.getName())) {
						items.add(it);
					}
				}
			}
			map.put(o, items);
		}
	}
	
	
	/**
	 * Method for passing the data to FileWriter
	 */
	public void saveData(String fileName) {
		FileWriter fw = new FileWriter();
		fw.insertData(this, fileName);
	}
	
	
}
