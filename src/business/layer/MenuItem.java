package business.layer;

import java.io.Serializable;

public class MenuItem implements Serializable{
	
	private String name;
	private double price;

	public double getPrice() {
		return price;
	}

	public void setPrice(double price) {
		this.price = price;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}
	
	
	public void computePrice() {
		
	}
	
}
