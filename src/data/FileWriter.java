package data;

import java.io.*;
import java.lang.*;
import java.util.List;

import com.itextpdf.text.Document;
import com.itextpdf.text.DocumentException;
import com.itextpdf.text.Paragraph;
import com.itextpdf.text.pdf.PdfPCell;
import com.itextpdf.text.pdf.PdfPTable;
import com.itextpdf.text.pdf.PdfWriter;

import business.layer.Order;
import business.layer.Restaurant;

/**
 * 
 * @author Mihai
 *A class for writing data in files
 */
public class FileWriter {
	
	/**
	 * Method to write data in pdf format(the Bills)
	 */
	public void writeFile(Order order, double price) {
		Document document = new Document();
		try {
			PdfWriter writer = PdfWriter.getInstance(document, new FileOutputStream("Bill"+order.getOrderId()+".pdf"));
			document.open();
			PdfPTable table = new PdfPTable(4);
			PdfPCell c1 = new PdfPCell(new Paragraph("Order"));
			PdfPCell c2 = new PdfPCell(new Paragraph("Table"));
			PdfPCell c3 = new PdfPCell(new Paragraph("Date"));
			PdfPCell c4 = new PdfPCell(new Paragraph("Price"));
			table.addCell(c1);
			table.addCell(c2);
			table.addCell(c3);
			table.addCell(c4);
				table.addCell(order.getStringOrder());
				table.addCell(String.valueOf(order.getTable()));
				table.addCell(String.valueOf(order.getDate()));
				table.addCell(String.valueOf(price));
			document.add(table);
			document.close();
		}catch(DocumentException e) {
			e.printStackTrace();
		}catch(FileNotFoundException e) {
			e.printStackTrace();
		}finally {
		}
	}
	
	/**
	 *Method to load information using serialization
	 */
	public void insertData(Restaurant r, String fileName) {
		try {
			ObjectOutputStream os = new ObjectOutputStream(new FileOutputStream(fileName));
			os.writeObject(r);
		} catch (FileNotFoundException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
	}
	
	
	
	
}
