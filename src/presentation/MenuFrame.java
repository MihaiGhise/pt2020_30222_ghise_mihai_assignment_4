package presentation;

import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.JTable;
import javax.swing.border.EmptyBorder;
/**
 * 
 * @author Mihai
 *Class used to help display the menu and orders
 */
public class MenuFrame extends JFrame{
	
	private JPanel contentPane;

	
	public MenuFrame(JTable table) {
		JFrame frame = new JFrame("Administrator");
		frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		frame.setBounds(100, 100, 350, 350);
		contentPane = new JPanel();
		contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
		frame.setContentPane(contentPane);
		contentPane.setLayout(null); 
		contentPane.add(table);
		frame.setVisible(true);
	}

}
