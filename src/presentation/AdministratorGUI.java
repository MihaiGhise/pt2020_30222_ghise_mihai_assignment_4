package presentation;

import java.awt.EventQueue;
import java.awt.Font;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.sql.DriverManager;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;
import java.util.Vector;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JTable;
import javax.swing.JTextField;
import javax.swing.border.EmptyBorder;

import business.layer.IRestaurantProcessing;
import business.layer.MenuItem;

/**
 * 
 * @author Mihai
 *Clasa de creeare a interfetei grafice "Administrator"
 */

public class AdministratorGUI extends JFrame implements IRestaurantProcessing{
	
	private JPanel contentPane;
	private List<MenuItem> items = new ArrayList<MenuItem>();
	private WaiterGUI waiter;
	
	public AdministratorGUI(String fileName) {
		JFrame frame = new JFrame("Administrator");
		frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		frame.setBounds(100, 100, 450, 350);
		contentPane = new JPanel();
		contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
		frame.setContentPane(contentPane);
		contentPane.setLayout(null); 
		//Creating the 3 text fields
		JTextField tf1 = createTextField(63, 30, 185, 35);
		JTextField tf2 = createTextField(63, 85, 185, 35);
		//JTable table = createTable(63, 143, 185, 78, items);
		createAddButton(297, 37, 85, 21, tf1);
		createUpdateButton(297, 84, 85, 21, tf1, tf2);
		createDeleteButton(297, 200, 85, 21, tf1);
		createViewButton(297, 140, 85, 21);
		JButton btnAdd = new JButton("AddWaiter");
		btnAdd.setBounds(297, 260, 85, 21);
		btnAdd.addActionListener((ActionListener) new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				WaiterGUI waiter1 = new WaiterGUI(items, fileName);
				//waiter1.createComboBox(63, 85, 185, 35, items);
				setWaiter(waiter1);
			}
		});
		contentPane.add(btnAdd);
		frame.setVisible(true);
	}
	
	public void setWaiter(WaiterGUI waiter) {
		this.waiter=waiter;
	}
	
	public WaiterGUI getWaiter() {
		return waiter;
		
	}
	
	
	private JTextField createTextField(int coordinate1, int coordinate2, int coordinate3, int coordinate4) {
		JTextField textField = new JTextField();
		textField.setBounds(coordinate1, coordinate2, coordinate3, coordinate4);
		contentPane.add(textField);
		textField.setColumns(10);
		return textField;
	}
	
	private JTable createTable(int coordinate1, int coordinate2, int coordinate3, int coordinate4) {
		JTable table = new JTable(items.size(), 2) ;
		table.setBounds(coordinate1, coordinate2, coordinate3, coordinate4);
		//contentPane.add(table);
		return table;
	}
	
	private void createAddButton(int coordinate1, int coordinate2, int coordinate3, int coordinate4, JTextField item) {
		JButton btnAdd = new JButton("Add");
		btnAdd.setBounds(coordinate1, coordinate2, coordinate3, coordinate4);
		btnAdd.addActionListener((ActionListener) new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				MenuItem menuItem = new MenuItem();
				menuItem.setName(parse(item.getText(), "\\D+"));
				menuItem.setPrice(Double.parseDouble(parse(item.getText(), "\\d+")));
				addItem(menuItem);
			}
		});
		contentPane.add(btnAdd);
	}
	
	private void createUpdateButton(int coordinate1, int coordinate2, int coordinate3, int coordinate4, JTextField oldItem, JTextField newItem) {
		JButton btnAdd = new JButton("Update");
		btnAdd.setBounds(coordinate1, coordinate2, coordinate3, coordinate4);
		btnAdd.addActionListener((ActionListener) new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				System.out.println(oldItem.getText());
				System.out.println(parse(newItem.getText(), "\\D+"));
				System.out.println(Double.parseDouble(parse(newItem.getText(), "\\d+")));
				updateItem(oldItem.getText(), parse(newItem.getText(), "\\D+"), Double.parseDouble(parse(newItem.getText(), "\\d+")));
			}
		});
		contentPane.add(btnAdd);
	}
	
	private void createDeleteButton(int coordinate1, int coordinate2, int coordinate3, int coordinate4, JTextField item) {
		JButton btnAdd = new JButton("Delete");
		btnAdd.setBounds(coordinate1, coordinate2, coordinate3, coordinate4);
		btnAdd.addActionListener((ActionListener) new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				deleteItem(item.getText());
			}
		});
		contentPane.add(btnAdd);
	}
	
	private void createViewButton(int coordinate1, int coordinate2, int coordinate3, int coordinate4) {
		JButton btnNewButton_1 = new JButton("View");
		btnNewButton_1.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				JTable table = createTable(100, 10, 150, 150);
				int row = 0;			
				for(MenuItem it : items) {
					table.setValueAt(it.getName(), row, 0);
					table.setValueAt(it.getPrice(), row, 1);
					System.out.println(it.getName());
					row++;
			}
				MenuFrame menu = new MenuFrame(table);
			}
		});
		btnNewButton_1.setBounds(coordinate1, coordinate2, coordinate3, coordinate4);
		contentPane.add(btnNewButton_1);
	}
	
	public List<MenuItem> getMenuItems() {
		return this.items;
	}
	
	public void addItem(MenuItem item) {
		items.add(item);
		//waiter.setMenuItems(items);
		//System.out.println(waiter.getMenuItems().get(0));
	}
	
	public void updateItem(String oldItem, String newItem, double newPrice) {
		for(MenuItem it : items) {
			if(it.getName().equals(oldItem)) {
				System.out.println("a gasit aitemul");
				it.setName(newItem);
				it.setPrice(newPrice);
			}
			for(MenuItem t : items) {
				if(it.getName().equals(oldItem)) {
					System.out.println(t.getName());
				}}
		}
	}
	
	public void deleteItem(String item) {
		MenuItem toDelete = new MenuItem();
		for(MenuItem mi : items) {
			if(mi.getName().equals(item)) {
				toDelete=mi;
				System.out.println("del: "+mi.getName()+" item: "+item);
			}
		}
		items.remove(toDelete);
	}
	
	private String parse(String produs, String pattern) {
	     int count = 0;
	     String comp = null;
	     Pattern polyFormat = Pattern.compile(pattern);
	     Matcher m = polyFormat.matcher(produs);
	     String s = new String();
	     while (m.find()){
				if (m.group().length() != 0){
					if(pattern.equals("\\d+")) {
						comp =m.group().trim();
						}
					else {
						comp = m.group().trim();
					}
					
	}
	}
	     return comp;
	}
	
}
