package presentation;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.ArrayList;
import java.util.List;
import java.util.Random;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import javax.swing.JButton;
import javax.swing.JComboBox;
import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.JTable;
import javax.swing.JTextField;
import javax.swing.border.EmptyBorder;

import business.layer.IRestaurantProcessing;
import business.layer.MenuItem;
import business.layer.Order;
import business.layer.Restaurant;
import data.FileWriter;

/**
 * 
 * @author Mihai
 *Clasa de creeare a interfetei grafice "Waiter"
 */

public class WaiterGUI extends JFrame implements IRestaurantProcessing {
	
	private JPanel contentPane;
	private JTextField textField;
	private JTable table;
	private List<Order> orders = new ArrayList<Order>();
	private List<MenuItem> menuItems = new ArrayList<MenuItem>();
	private int orderId = 0;
	private AdministratorGUI administrator;
	WaiterGUI waiter;
	
	public WaiterGUI(List<MenuItem> list, String fileName) {
		JFrame frame = new JFrame("Waiter");
		frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		frame.setBounds(100, 100, 450, 350);
		contentPane = new JPanel();
		contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
		frame.setContentPane(contentPane);
		contentPane.setLayout(null);
		this.administrator=administrator;
		JTextField textField = createTextField(63, 51, 185, 35);
		//this.menuItems = administrator.getMenuItems();
		menuItems = list;
		JComboBox comboBox = createComboBox(63, 85, 185, 35, menuItems);
		createComboBox(63, 85, 185, 35, menuItems);
		createAddtoOrderButton(297, 200, 85, 21, textField, comboBox);
		createAddButton(297, 58, 85, 21, textField);
		createViewButton(297, 116, 85, 21);
		createComputePriceButton(297, 168, 85, 21, textField);
		createComputeBillButton(297, 218, 85, 21, textField);
		createRestaurant(297, 250, 85, 21, fileName);
		frame.setVisible(true);
	}
	
	
	public WaiterGUI() {
		JFrame frame = new JFrame("Waiter");
		frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		frame.setBounds(100, 100, 450, 350);
		contentPane = new JPanel();
		contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
		frame.setContentPane(contentPane);
		contentPane.setLayout(null);
		
		JTextField textField = createTextField(63, 51, 185, 35);
		createComboBox(63, 85, 185, 35, menuItems);
		//createAddtoOrderButton(297, 200, 85, 21, comboBox, textField);
		createAddButton(297, 58, 85, 21, textField);
		createViewButton(297, 116, 85, 21);
		createComputePriceButton(297, 168, 85, 21, textField);
		createComputeBillButton(287, 218, 85, 21, textField);
		frame.setVisible(true);
	}
	 
	public void createComputeBillButton(int coordinate1, int coordinate2, int coordinate3, int coordinate4, JTextField textField) {
		JButton btnNewButton = new JButton("Bill");
		btnNewButton.setBounds(coordinate1, coordinate2, coordinate3, coordinate4);
		btnNewButton.addActionListener((ActionListener) new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				computeBill(Integer.parseInt(textField.getText()));
			}
		});
		contentPane.add(btnNewButton);
	}
	
	public JComboBox createComboBox(int coordinate1, int coordinate2, int coordinate3, int coordinate4, List<MenuItem> menuItems) {
		JComboBox comboBox = new JComboBox();
		comboBox.setBounds(coordinate1, coordinate2, coordinate3, coordinate4);
		contentPane.add(comboBox);
		for(MenuItem mi : menuItems) {
			comboBox.addItem(mi.getName());
		}
		return comboBox;
	}
	
	private JTextField createTextField(int coordinate1, int coordinate2, int coordinate3, int coordinate4) {
		JTextField textField = new JTextField();
		textField.setBounds(coordinate1, coordinate2, coordinate3, coordinate4);
		contentPane.add(textField);
		textField.setColumns(10);
		return textField;
	}
	
	private JTable createTable(int coordinate1, int coordinate2, int coordinate3, int coordinate4) {
		JTable table = new JTable(orders.size(), 4);
		table.setBounds(coordinate1, coordinate2, coordinate3, coordinate4);
		//contentPane.add(table);
		return table;
	}
	
	private void createAddButton(int coordinate1, int coordinate2, int coordinate3, int coordinate4, JTextField orderItems) {
		JButton btnNewButton = new JButton("Add");
		btnNewButton.setBounds(297, 58, 85, 21);
		btnNewButton.addActionListener((ActionListener) new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				addOrder(orderItems.getText());
			}
		});
		contentPane.add(btnNewButton);
	}
	
	private void createAddtoOrderButton(int coordinate1, int coordinate2, int coordinate3, int coordinate4, JTextField orderItems, JComboBox comboBox) {
		JButton btnNewButton = new JButton("AddToOrder");
		btnNewButton.setBounds(297, 200, 85, 21);
		btnNewButton.addActionListener((ActionListener) new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				orderItems.setText(addToOrder(orderItems, comboBox));
			}
		});
		contentPane.add(btnNewButton);
	}
	
	private void createViewButton(int coordinate1, int coordinate2, int coordinate3, int coordinate4) {
		JButton btnNewButton_1 = new JButton("View");
		btnNewButton_1.setBounds(297, 116, 85, 21);
		btnNewButton_1.addActionListener((ActionListener) new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				viewOrders();
			}
		});
		contentPane.add(btnNewButton_1);
	}
	
	private void createComputePriceButton(int coordinate1, int coordinate2, int coordinate3, int coordinate4, JTextField order) {
		JButton btnComputeBill = new JButton("Price");
		btnComputeBill.setBounds(297, 168, 85, 21);
		btnComputeBill.addActionListener((ActionListener) new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				System.out.println(order.getText());
				computePrice(order.getText());
			}
		});
		contentPane.add(btnComputeBill);
	}
	
	private void createRestaurant(int coordinate1, int coordinate2, int coordinate3, int coordinate4, String fileName) {
		JButton btnComputeBill = new JButton("Restaurant");
		btnComputeBill.setBounds(297, 250, 85, 21);
		waiter = this;
		btnComputeBill.addActionListener((ActionListener) new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				for(MenuItem o : menuItems) {
					System.out.println("din waiter: "+o.getName());
				}
				Restaurant restaurant = new Restaurant(menuItems, waiter, fileName);
			}
		});
		contentPane.add(btnComputeBill);
	}
	
	public String addToOrder(JTextField orderItems, JComboBox comboBox) {
		String text = new String();
		int index = comboBox.getSelectedIndex();
		text = orderItems.getText() + menuItems.get(index).getName()+ " ";
		return text;
	}
	
	public List<MenuItem> getMenuItems(){
		return menuItems;
		
	}
	
	public void setMenuItems(List<MenuItem> items) {
		menuItems = items;
	}
	
	private void viewOrders() {
		JTable table = createTable(100, 10, 150, 150);
		int row = 0;			
		for(Order it : orders) {
			table.setValueAt(it.getOrderId(), row, 0);
			table.setValueAt(it.getTable(), row, 1);
			table.setValueAt(it.getDate(), row, 2);
			table.setValueAt(it.getStringOrder(), row, 3);
			row++;
	}
		MenuFrame menu = new MenuFrame(table);
		
	}
	
	private double computePrice(String order) {
		double price = 0;
		List<String> orders = parse(order, "\\S+");
		for(String o : orders) {
		for(MenuItem mi : menuItems) {
				if(mi.getName().equals(o)) {
					price = price + mi.getPrice();
				}
			}
		}
		return price;
	}
	
	private void computeBill(int orderId) {
		FileWriter fw = new FileWriter();
		fw.writeFile(orders.get(orderId), computePrice(orders.get(orderId).getStringOrder()));
	}
	
	private void addOrder(String orderItems) {
		Order order = new Order();
		Random rand = new Random();
		order.setOrderId(orderId);
		orderId++;
		order.setTable(rand.nextInt(10-1)+1);
		order.setDate(rand.nextInt(31-1)+1);
		order.setStringOrder(orderItems);
		orders.add(order);
		System.out.println("adaugat: "+order.getStringOrder());
	}
	
	public List<Order> getOrders(){
		return orders;
	}
	
	public List<String> parse(String produs, String pattern) {
	     int count = 0;
	     List <String> comp = new ArrayList<String>();
	     Pattern polyFormat = Pattern.compile(pattern);
	     Matcher m = polyFormat.matcher(produs);
	     String s = new String();
	     while (m.find()){
				if (m.group().length() != 0){
					if(pattern.equals("\\S+")) {
						comp.add(m.group().trim());
						}
					
	}
	}
	     return comp;
	}
}
